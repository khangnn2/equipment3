using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlasmaGunController : Singleton<PlasmaGunController>
{
    public GameObject pnlUpgrade;
    public Text txtDame;
    public Text txtDispersion;
    public Text txtRateOfFire;
    public Text txtReloadSpeed;
    public Text txtAmmunition;
    
    //Show Info Gun
    public void ShowInfoGun(Gun g)
    {
        txtDame.text = g.damage.ToString();
        txtDispersion.text = g.dispersion.ToString();
        txtRateOfFire.text = g.rateOfFire.ToString() + " RPM";
        txtReloadSpeed.text = g.reloadSpeed.ToString() + "%";
        txtAmmunition.text = g.ammunition.ToString() + "/100";
    }


    //Event for two Buttons Upgrade
    public void SelectUpgrade()
    {
        pnlUpgrade.SetActive(true);
    }

    //Event for button Accept Upgrade
    public void AcceptUpgrade()
    {
        Upgrade();
        pnlUpgrade.SetActive(false);
    }

    //Perform the Upgrade
    public void Upgrade()
    {
        ShopGunController.Ins.cur_GunSelected.gun.damage += 25;
        ShopGunController.Ins.cur_GunSelected.gun.dispersion += 10;
        ShopGunController.Ins.cur_GunSelected.gun.rateOfFire += 100;
        ShopGunController.Ins.cur_GunSelected.gun.reloadSpeed += 10;
        if (ShopGunController.Ins.cur_GunSelected.gun.reloadSpeed >= 100)
        {
            ShopGunController.Ins.cur_GunSelected.gun.reloadSpeed = 100;
        }
        ShopGunController.Ins.cur_GunSelected.gun.ammunition += 20;
        if (ShopGunController.Ins.cur_GunSelected.gun.ammunition >= 100)
        {
            ShopGunController.Ins.cur_GunSelected.gun.ammunition = 100;
        }
        ShowInfoGun(ShopGunController.Ins.cur_GunSelected.gun);
          
    }

    //Event for button OK, button No,  Hide unused panels
    public void Resume()
    {
        pnlUpgrade.SetActive(false);
    }
}
